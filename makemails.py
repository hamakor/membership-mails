import csv
import datetime
import subprocess
import sys
import time

import jinja2


def readfile(filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        return list(reader)


def context(entry, all_years=tuple(str(year) for year in range(2018, 2022))):

    eng_name = '{first_name} {last_name}'.format(**entry)
    heb_name = entry['first_heb']
    email = entry['email_address']
    unpaid_years = [
        year for year in all_years
        if not entry[year].strip()
    ]
    del entry, all_years
    return locals()

if __name__ == '__main__':

    command = ['claws-mail', '--compose-from-file', '-']
    
    member_src = open('member-status-update-template.txt').read()
    member_template = jinja2.Template(member_src)

    rows = readfile(sys.argv[1])
    
    for i, row in enumerate(rows, 1):
        print(f'{i:3}/{len(rows)}:', row, flush=True)
        entry = context(row)
        if not entry:
            continue

        text = member_template.render(**entry)
        subprocess.run(command, input=text, text=True, check=True)

    
        
