To use this,
- Install the requirements (preferably in a virtual environment)
- Edit the template
- Get the existing members data. This is supposed to be a CSV file with columns "first_name", "last_name", "first_heb",
  "email_address",2018, 2019, 2020, 2021 -- where the year columns are empty if they aren't paid for
- Adapt everything to your liking. The test initially uploaded was for our 26.4.2021 General Assembly
- Run `python makemails.py data-file.csv`.

This assumes you have `claws-mail` installed, and will open a compose-mail window for every member, with their personalized message
in it. You just need to verify the contents and address, and hit "send" (for each member).
